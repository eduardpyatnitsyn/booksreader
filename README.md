# ТЗ #
# Приложение “Книги” #

Приложение предоставляет возможность чтения книг и состоит из следущих компонент:
1.	Каталог
2.	Интерфейс для чтения

## Каталог ##

Книги в каталог загружаются один раз любым способом и должны хранится в базе данных в защищенном виде. Тип защиты - на усмотрение разработчика (например, простейший XOR битового массива книги).

Структура базы данных:

При необходимости структу БД можно расширить.

В интерфейсе каталога пользователь отображаются имя автора, название книги и жанр в табличном виде. 

По клику на строку книги должен открываться режим просмотра

## Интерфейс для чтения ##

Интерфейс предоставляет собой возможность читать и перелистывать страницы книги. Выбор шрифта на усмотрение.